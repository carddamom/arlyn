# Arlyn

This is a list of project templates for yeoman.

## Installation

To install this templates, you must first install yeoman.
And then install using yarn, the templates you want to use.

## Usage

As I've said Arlyn, is a bunch of templates for yeoman, the point of then
is that one of then alone, won't create a complete project, since they are
made, to perform simple scaffolding tasks, like create base files (README, LICENSE, etc),
adding frameworks, etc. In a microkernel fashion.

As such, this is also written as one big monorepo, that uses lerna and yarn workspaces, in spite of
this each one tries not to depend on the others.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

None yet.

## Credits

- JHipster, for being a shitty template, that does tons of stuff in one go, that exact oposite of Arlyn.

## License

All of the templates are licensed under a Apache 2.0 License, meaning that they can be used for
everything.
